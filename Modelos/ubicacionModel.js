var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ubicacionSchema = new Schema({
    Nombre: String,
    Longitud: Number,
    Latitud: Number,
    CodigoPostal: Number,
    Created_at:{ type: Date, required: true, default: Date.now},
    Pais: String

},{
    versionKey: false
});

module.exports = mongoose.model('Ubicacion',ubicacionSchema);