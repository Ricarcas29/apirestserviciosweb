var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var publicacionSchema = new Schema({
    Contenido: String,
   Created_at: { type:Date, required:true, default:Date.now},
   IdUbicacion: { type: Schema.ObjectId, reference: 'Ubicacion'}

},{
    versionKey : false
});

module.exports = mongoose.model('Publicacion',publicacionSchema);