//quien gestiona las solicitudes y gestiona las respuestas
var express = require("express");
var bodyParser = require("body-parser");
var methodOverride = require("method-override");
var mongoose = require("mongoose");


//llama al metodo Express
var app = express();

//generamos la variable app
//definimos cierta configuracion

app.use(bodyParser());
app.use(methodOverride());

//definir el puerto por el cual vamos a escuchar
app.listen(3000,function(){
    console.log("Nuestro servidor escucha por el puerto 3000");
})


//conexion a la base de datos

mongoose.connect('mongodb://localhost/GreensTeacher',function(err, res){

if(err){
    console.log('ERROR: connecting to Database. '+err);
}else{
    console.log('Conectado  MongoDB');
}
});









//Definir el enrutamiento de las solicitudes
var ControladorUbicacion = require('./Controladores/ubicacionControlador');
var router = express.Router();
//Indicar al enrutamiento las URI por las cuales escuchara mi servicio

router.get('/',function(req, res){

    res.send("Hola mundo del servicio nodejs");

})

//definir los set point para cada una de las operaciones
//Agregar ubicacion
router.post('/API/ubicacion/AddUbicacion', function(req, res){
    ControladorUbicacion.addUbicacion(req, function(data){
        res.send(data);
    });
});
//update ubicacion
router.put('/API/ubicacion/updateUbicacion', function(req, res){
    ControladorUbicacion.updateUbicacion(req, function(data){
        res.send(data);
    });
});


//eliminar ubicacion
router.delete('/API/ubicacion/DeleteUbicacion/:id', function(req, res){
    ControladorUbicacion.deleteUbicacion(req, function(data){
        
        res.send(data);
    });
});
//buscar ubicacion
router.get('/API/ubicacion/findubicacion/:id', function(req, res){
    ControladorUbicacion.findByIdUbicacion(req, function(data){
        console.log(req);
        res.send(data);
    });
});

//buscar ubicaciones
router.get('/API/ubicacion/findAllubicacion', function(req, res){
    ControladorUbicacion.findAllUbicacion(req, function(data){
        res.send(data);
    });
});


//Revisar autenticacion





app.use(router);

