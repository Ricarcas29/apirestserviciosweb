var mongoose = require('mongoose');
require('../Modelos/ubicacionModel.js');
require('../Modelos/publicacionModel.js');
//generar las variables enlaces
var ubicacionModel = mongoose.model('Ubicacion');
var publicacionModel = mongoose.model('Publicacion');
//Definir la implementacion de las diferentes funcionalidades que vamos a ofrecer a nuestro servicio
//agrear 
exports.addUbicacion = function(req, callback){

    var objUbicacion = new ubicacionModel();
    objUbicacion.Nombre = req.body.Nombre;
    objUbicacion.Longitud = req.body.Longitud;
    objUbicacion.Latitud = req.body.Latitud;
    objUbicacion.CodigoPostal = req.body.CodigoPostal;
    objUbicacion.Pais = req.body.Pais;
    objUbicacion.save(function(err, retorno){
        if(err) callback({estado:{codigo:2, respuesta:err.message}});
        callback({estado:{codigo:1,respuesta:'proceso exitoso'}, Ubicacion: retorno});
    });
};
//actualizar
exports.updateUbicacion = function(req, callback){
    //identificaremos el id
    ubicacionModel.findById(req.params.id,function(err,ubicacionID){
        //gestionaremos los datos a actualizar
            ubicacionID.Nombre = req.body.Nombre;
            ubicacionID.Longitud = req.body.Longitud;
            ubicacionID.Latitud = req.body.Latitud;
            ubicacionID.Pais = req.body.Pais;
            ubicacionID.CodigoPostal = req.body.CodigoPostal;
        //salvamos los datos nuevos
        ubicacionID.save(function(err, resultadoUpdate){
            if(err) callback({estado:{codigo:2, respuesta:err.message}});
            callback({estado:{codigo:1,respuesta:'proceso exitoso'},ubicacion: resultadoUpdate});
        });
    });
};
//eliminar persona
exports.deleteUbicacion = function(req, callback){
    ubicacionModel.findById(req.params.id,function(err,ubicacionID){
        //salvamos los datos nuevos
        ubicacionID.remove(function(err){
            if(err) callback({estado:{codigo:2, respuesta:err.message}});
            callback({estado:{codigo:1,respuesta:'proceso exitoso'}});
        });
    });
};
//buscar una persona por id
exports.findByIdUbicacion = function(req, callback){
    ubicacionModel.findById(req.params.id,function(err,ubicacionID){
            if(err) callback({estado:{codigo:2, respuesta:err.message}});
            callback({estado:{codigo:1,respuesta:'proceso exitoso'},ubicacion: ubicacionID});
    });
};
//buscar todas las personas
exports.findAllUbicacion = function(req, callback){

    
    ubicacionModel.find({},function(err,ubicacionesID){
        if(err) callback({estado:{codigo:2, respuesta:err.message}});
        callback({ubicaciones: ubicacionesID});

});



};